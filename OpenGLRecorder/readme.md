
## Android Camera OpenGL添加水印并录制视频
>- 简单的视频录制，我们可以使用[MediaRecorder](https://developer.android.com/reference/android/media/MediaRecorder)，具体示例可以参考[Gitee: Camera2VideoJava](https://gitee.com/chenjim/CameraDemo/tree/master/Camera2VideoJava)
>- 本文将介绍采集Camera的预览数据，然后通过[OpenGL](https://developer.android.com/guide/topics/graphics/opengl)添加水印、贴纸、美颜滤镜等后渲染到[GLSurfaceView](https://developer.android.com/reference/android/opengl/GLSurfaceView)，再用[MediaCodec](https://developer.android.com/reference/android/media/MediaCodec)硬编码后通过[MediaMuxer](https://developer.android.com/reference/android/media/MediaMuxer)写入MP4文件。
> 源码地址: [Gitee: OpenGLRecorder](https://gitee.com/chenjim/CameraDemo/tree/master/OpenGLRecorder)

大致流程如下，详细可以参见相应代码连接
1. [CameraGlView](https://gitee.com/chenjim/CameraDemo/blob/master/OpenGLRecorder/app/src/main/java/com/chenjim/glrecorder/CameraGlView.java) 用来显示Camera预览的View
CameraGlView extends GLSurfaceView
2. 创建SurfaceTexture，用来显示Camera预览，参见[CameraRenderer.java](https://gitee.com/chenjim/CameraDemo/blob/master/OpenGLRecorder/app/src/main/java/com/chenjim/glrecorder/CameraRenderer.java)
mSurfaceTexture = new SurfaceTexture(mTextures[0]);
...
mCameraHelper.startPreview(mSurfaceTexture);

3. [CameraFilter.java](https://gitee.com/chenjim/CameraDemo/blob/master/OpenGLRecorder/app/src/main/java/com/chenjim/glrecorder/filter/CameraFilter.java)，通过OpenGL将Camera数据写入FBO(Frame Buffer Object 帧缓存)
4. [TimeFilter.java](https://gitee.com/chenjim/CameraDemo/blob/master/OpenGLRecorder/app/src/main/java/com/chenjim/glrecorder/filter/TimeFilter.java)，通过OpenGL在FBO上添加时间水印。
可以参考此处添加贴纸、美颜等
5. [ScreenFilter.java](https://gitee.com/chenjim/CameraDemo/blob/master/OpenGLRecorder/app/src/main/java/com/chenjim/glrecorder/filter/ScreenFilter.java)，将FBO绘制到mSurfaceTexture
6. [MediaRecorder.java](https://gitee.com/chenjim/CameraDemo/blob/master/OpenGLRecorder/app/src/main/java/com/chenjim/glrecorder/MediaRecorder.java)，用MediaCodec和EGL对Surface的内容采集编码为avc并写入到MP4文件


参考文章:
- [OpenGL ES SDK for Android: High Quality Text Rendering](https://arm-software.github.io/opengl-es-sdk-for-android/high_quality_text.html)
- [Github：opengl-es-sdk-for-android/HighQualityTextJava](https://github.com/ARM-software/opengl-es-sdk-for-android/tree/master/samples/advanced_samples/HighQualityTextJava)
- [Github：android-openGL-canvas](https://github.com/ChillingVan/android-openGL-canvas)
- [Github:Media for Mobile is a set of easy to use components and API for a wide range of media scenarios such as video editing and capturing](https://github.com/INDExOS/media-for-mobile)
- [VideoRecorder高性能任意尺寸视频录制 断点录制 离屏录制 ](https://github.com/lll-01/VideoRecorder) 录制时的Canvas API支持 实时滤镜，[相关资料介绍Link](https://github.com/lll-01/VideoRecorder/blob/master/app/doc/%E7%9B%B8%E5%85%B3%E8%B5%84%E6%96%99.md)
- [抖音录制视频预习资料](https://www.jianshu.com/p/6aaa44072272)

package com.chenjim.glrecorder;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.EGL14;
import android.opengl.EGLContext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.Log;

import com.chenjim.glrecorder.filter.CameraFilter;
import com.chenjim.glrecorder.filter.ScreenFilter;
import com.chenjim.glrecorder.filter.TimeFilter;

import java.io.IOException;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class CameraRenderer implements GLSurfaceView.Renderer,
        SurfaceTexture.OnFrameAvailableListener, Camera.PreviewCallback {
    private ScreenFilter mScreenFilter;
    private CameraGlView mView;
    private CameraHelper mCameraHelper;
    private SurfaceTexture mSurfaceTexture;
    private float[] mtx = new float[16];
    private int[] mTextures;
    private CameraFilter mCameraFilter;
    private MediaRecorder mMediaRecorder;
    private TimeFilter timeFilter;
    private int mHeigh;
    private int mWidth;
    private String TAG = CameraRenderer.class.getSimpleName();

    public CameraRenderer(CameraGlView cameraGlView) {
        mView = cameraGlView;
    }

    /**
     * 在Surface被创建时回调，用来配置 View 的 OpenGL ES 环境，只会被回调一次；
     *
     * 一些初始化工作
     * @param gl
     * @param config
     */
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //初始化准备camera
        mCameraHelper = new CameraHelper(Camera.CameraInfo.CAMERA_FACING_BACK);
        mCameraHelper.setPreviewCallback(this);
        //准备好摄像头绘制的画布
        //通过openGL创建一个纹理id
        mTextures = new int[1];
        //偷懒 这里可以不配置 （当然 配置了也可以）
        //纹理
        GLES20.glGenTextures(mTextures.length, mTextures, 0);
        mSurfaceTexture = new SurfaceTexture(mTextures[0]);
        //
        mSurfaceTexture.setOnFrameAvailableListener(this);
        //注意：必须在gl线程操作opengl
        mCameraFilter = new CameraFilter(mView.getContext());
        // 将FBO绘制到mSurfaceTexture
        mScreenFilter = new ScreenFilter(mView.getContext());
        // 贴纸，美颜
        timeFilter = new TimeFilter(mView.getContext());

        //渲染线程的EGL上下文
        EGLContext eglContext = EGL14.eglGetCurrentContext();

        mMediaRecorder = new MediaRecorder(mView.getContext(), "/sdcard/a.mp4",
                CameraHelper.HEIGHT, CameraHelper.WIDTH, eglContext);
    }

    /**
     * 在每次Surface尺寸变化时回调，例如当设备的屏幕方向发生改变时
     *
     * @param gl
     * @param width
     * @param height
     */
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        mWidth = width;
        mHeigh = height;

        //开启预览
        mCameraHelper.startPreview(mSurfaceTexture);
        //准备
        mCameraFilter.onReady(width, height);
        mScreenFilter.onReady(width, height);
        timeFilter.onReady(width, height);
    }

    /**
     * 开始画画吧
     *
     * 这个是最重要的方法，没有之一。前面两个，只会在surface created时调一次
     * 而此方法是用来绘制每帧的，所以每次刷新都会被调一次，所有的绘制都发生在这里。
     *
     * @param gl
     */
    @Override
    public void onDrawFrame(GL10 gl) {
        long time0 = SystemClock.elapsedRealtime();
        //配置屏幕
        //清理屏幕 :告诉opengl 需要把屏幕清理成什么颜色
        GLES20.glClearColor(0, 0, 0, 0);
        //执行上一个：glClearColor配置的屏幕颜色
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        // 把摄像头的数据先输出来
        // 更新纹理，然后我们才能够使用opengl从SurfaceTexure当中获得数据 进行渲染
        mSurfaceTexture.updateTexImage();
        //surfaceTexture 比较特殊，在opengl当中 使用的是特殊的采样器 samplerExternalOES （不是sampler2D）
        //获得变换矩阵
        mSurfaceTexture.getTransformMatrix(mtx);
        //
        mCameraFilter.setMatrix(mtx);
        //责任链
        int id = mCameraFilter.onDrawFrame(mTextures[0]);
        //加效果滤镜
        // id  = 效果1.onDrawFrame(id);
        // id = 效果2.onDrawFrame(id);
        //....

        id = timeFilter.onDrawFrame(id);

        //加完之后再显示到屏幕中去
        mScreenFilter.onDrawFrame(id);

        //进行录制
        mMediaRecorder.encodeFrame(id, mSurfaceTexture.getTimestamp());

        Log.d(TAG, "onDrawFrame time:" + (SystemClock.elapsedRealtime() - time0));
    }

    /**
     * surfaceTexture 有一个有效的新数据的时候回调
     *
     * @param surfaceTexture
     */
    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        mView.requestRender();
    }

    public void onSurfaceDestroyed() {
        mCameraHelper.stopPreview();
    }

    public void startRecord(float speed) {
        try {
            mMediaRecorder.start(speed);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopRecord() {
        mMediaRecorder.stop();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        // data 可以做人脸识别

    }

}
